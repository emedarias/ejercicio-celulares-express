# ejercicio-celulares-express

# Pagina de celulares

Este  proyecto busca que el usuario envie parametros por la URL y que devuelca:

* El celular de mayor precio.
* El celular de menor precio.
* Todos los celulares ordenados por grama baja, media y alta.
