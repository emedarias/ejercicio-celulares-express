const express=require('express');
const app = express();

//Array con objetos celulares, con diferentes marcas, precios, gamas, precios, etc.
const celulares = [{'Marca':'Samsung','Gama':'Alta','Modelo':'Galaxy','Pantalla':'Super-Re-Copada','SO':'Android','Precio':'40000'},
					{'Marca':'Motorola','Gama':'Media','Modelo':'G6','Pantalla':'Re-copada','SO':'Android','Precio':'18000'},
					{'Marca':'Nokia','Gama':'Baja','Modelo':'1100','Pantalla':'Re-copada','SO':'Android','Precio':'9000'},
					{'Marca':'Motorola','Gama':'Media','Modelo':'G4','Pantalla':'Re-copada','SO':'Android','Precio':'16000'}]

//Cuando la direccion sea solo localhost:9090 va a llamar a la funcion OrdenarPorGama.
app.get('/',OrdenarPorGama);

//Cuando la direccion sea solo localhost:9090/mayor va a llamar a la funcion Mayor.
app.get('/mayor',Mayor);

//Cuando la direccion sea solo localhost:9090/menor va a llamar a la funcion Menor.
app.get('/menor',Menor);

function OrdenarPorGama(request, response)
	{
		//Creo 3 Arrays para diferenciar si los distintos celulares son de gama alta, media o baja y los guardo por separado.
		const alta = new Array;
		const media = new Array;
		const baja = new Array;

		//Navego el vector que tiene todos los celulares.
		for(cel of celulares)
			{
				//Si el celular es de gama Alta, lo guarda en el Array 'alta'
				if(cel['Gama']==='Alta')
					{
						alta.push(cel);
					}
				//Si el celular es de gama Media, lo guarda en el Array 'media'
				if(cel['Gama']==='Media')
					{	
						media.push(cel);		
					}
				//Si el celular es de gama Baja, lo guarda en el Array 'baja'
				if(cel['Gama']==='Baja')
					{
						baja.push(cel);
					}
			}
		
		//Creo la variable que tiene la estructura del html basica, sin cerrar el </body> ni el </html>.
		let mensaje="<!DOCTYPE html><html><head><meta charset='utf-8'><meta name='viewport' content='width=device-width, initial-scale=1, user-scalable=no'><title>Tabla responsive</title><link rel='stylesheet' type='text/css' href='css/estilo.css'></head><body>";
		
		//Le agrego a la variable mensaje, el titulo 'Gama Alta'.
		mensaje+="<h1>Gama Alta</h1>";

		//Navego el Array que tiene todos los celulares de gama alta.
		for(cel of alta)
			{
				//Por cada celular en el Array alta, agrego una lista con todos los datos del mismo a 'mensaje'.
				mensaje+="<ul><li><b>Marca: </b>"+cel['Marca']+".</li><li><b>Gama: </b>"+cel['Gama']+".</li><li><b>Modelo: </b>"+cel['Modelo']+".</li><li><b>Pantalla: </b>"+cel['Pantalla']+".</li><li><b>SO: </b>"+cel['SO']+".</li><li><b>Precio: </b>$"+cel['Precio']+".</li></ul>";
			}
		
		//Le agrego a la variable mensaje, el titulo 'Gama Media'.
		mensaje+="<h1>Gama Media</h1>";

		for(cel of media)
			{
				//Por cada celular en el Array media, agrego una lista con todos los datos del mismo a 'mensaje'.
				mensaje+="<ul><li><b>Marca: </b>"+cel['Marca']+".</li><li><b>Gama: </b>"+cel['Gama']+".</li><li><b>Modelo: </b>"+cel['Modelo']+".</li><li><b>Pantalla: </b>"+cel['Pantalla']+".</li><li><b>SO: </b>"+cel['SO']+".</li><li><b>Precio: </b>$"+cel['Precio']+".</li></ul>";
			}

		//Le agrego a la variable mensaje, el titulo 'Gama Baja'.
		mensaje+="<h1>Gama Baja</h1>";

		for(cel of baja)
			{
				//Por cada celular en el Array baja, agrego una lista con todos los datos del mismo a 'mensaje'.
				mensaje+="<ul><li><b>Marca: </b>"+cel['Marca']+".</li><li><b>Gama: </b>"+cel['Gama']+".</li><li><b>Modelo: </b>"+cel['Modelo']+".</li><li><b>Pantalla: </b>"+cel['Pantalla']+".</li><li><b>SO: </b>"+cel['SO']+".</li><li><b>Precio: </b>$"+cel['Precio']+".</li></ul>";
			}
		
		//Le agrego a la variable mensaje el cierre del body y del html, despues de agregar todos los listados de gama alta, media y baja.
		mensaje+="</body></html";

		//Envio la variable 'mensaje' como respuesta.
		response.send(mensaje);
	}

function Mayor(request, response)
	{
		//Defino que el valor mayor es 0, para comparar con todos los precios.
		let mayor = 0;

		//Creo la variable que tiene la estructura del html basica, sin cerrar el </body> ni el </html>.
		let mensaje="<!DOCTYPE html><html><head><meta charset='utf-8'><meta name='viewport' content='width=device-width, initial-scale=1, user-scalable=no'><title>Tabla responsive</title><link rel='stylesheet' type='text/css' href='css/estilo.css'></head><body>";

		//Creo la variable seleccionado donde voy a guradar los datos del elemento con el mayor precio.
		let seleccionado="";

		//Navego el Array de los celulares.
		for(cel of celulares)
			{
				//Consulto si el precio del celular es mayor al numero guardado en la variable 'mayor'.
				if(Number(cel['Precio'])>mayor)
					{
						//La variable 'mayor' toma el precio del celular.
						mayor=Number(cel['Precio']);

						//A la variable seleccionado le agregamos el titulo 'Mas Caro'.
						seleccionado="<h1>Mas Caro</h1>";

						//Agrego una lista con todos los datos del mismo a 'seleccionado'.
						seleccionado+="<ul><li><b>Marca: </b>"+cel['Marca']+".</li><li><b>Gama: </b>"+cel['Gama']+".</li><li><b>Modelo: </b>"+cel['Modelo']+".</li><li><b>Pantalla: </b>"+cel['Pantalla']+".</li><li><b>SO: </b>"+cel['SO']+".</li><li><b>Precio: </b>$"+cel['Precio']+".</li></ul>";
					}
			}

		//Una vez terminado el ciclo, agrego a 'mensaje' los datos del celular con mayor precio guardado y cierron </body> y </html>.
		mensaje+=seleccionado+"</body></html";

		//Envio la variable 'mensaje' como respuesta.
		response.send(mensaje);
	}	

function Menor(request, response)
	{
		//Defino que el valor menor es 999999999999, para comparar con todos los precios.
		let menor = 999999999999;

		//Creo la variable que tiene la estructura del html basica, sin cerrar el </body> ni el </html>.
		let mensaje="<!DOCTYPE html><html><head><meta charset='utf-8'><meta name='viewport' content='width=device-width, initial-scale=1, user-scalable=no'><title>Tabla responsive</title><link rel='stylesheet' type='text/css' href='css/estilo.css'></head><body>";

		//Creo la variable seleccionado donde voy a guradar los datos del elemento con el mayor precio.
		let seleccionado="";

		//Navego el Array de los celulares.
		for(cel of celulares)
			{
				
				//Consulto si el precio del celular es menor al numero guardado en la variable 'menor'.
				if(Number(cel['Precio'])<menor)
					{
						//La variable 'menor' toma el precio del celular.
						menor=Number(cel['Precio']);

						//A la variable seleccionado le agregamos el titulo 'Mas Barato'.
						seleccionado="<h1>Mas Barato</h1>";

						//Agrego una lista con todos los datos del mismo a 'seleccionado'.
						seleccionado+="<ul><li><b>Marca: </b>"+cel['Marca']+".</li><li><b>Gama: </b>"+cel['Gama']+".</li><li><b>Modelo: </b>"+cel['Modelo']+".</li><li><b>Pantalla: </b>"+cel['Pantalla']+".</li><li><b>SO: </b>"+cel['SO']+".</li><li><b>Precio: </b>$"+cel['Precio']+".</li></ul>";
					}
			}

		//Una vez terminado el ciclo, agrego a 'mensaje' los datos del celular con mayor precio guardado y cierron </body> y </html>.
		mensaje+=seleccionado+"</body></html";

		//Envio la variable 'mensaje' como respuesta.
		response.send(mensaje);
	}


function ListoElPollo()
	{
		console.log('A comer en el puerto 9090');
	}

//Funcion para verificar que esta funcionando express, de ser asi, llama a la funcion ListoElPollo.
app.listen(9090, ListoElPollo);